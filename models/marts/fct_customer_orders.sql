with orders as(
    select * from {{ ref('stg_orders')}}
)
,
customers as (
    select * from {{ ref('stg_customers')}}
)
, payment as (
    select * from {{ ref('stg_payments')}}
)
, paid_orders as(
    select * from {{ ref('paid_orders')}}
)

select
p.*,
ROW_NUMBER() OVER (ORDER BY p.order_id) as transaction_seq,
ROW_NUMBER() OVER (PARTITION BY customer_id ORDER BY p.order_id) as customer_sales_seq,
case when(rank() over(partition by customer_id order by p.order_placed_at, p.order_id)) = 1 then 'new' else 'return' end as nvsr,
x.clv_bad as customer_lifetime_value,
first_value(p.order_placed_at) over(partition by customer_id order by p.order_placed_at) as fdos
FROM paid_orders p
LEFT OUTER JOIN 
(
        select
        p.order_id,
        sum(t2.total_amount_paid) as clv_bad
    from paid_orders p
    left join paid_orders t2 on p.customer_id = t2.customer_id and p.order_id >= t2.order_id
    group by 1
    order by p.order_id
) x on x.order_id = p.order_id
ORDER BY order_id
