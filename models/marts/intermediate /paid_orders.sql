with paid_orders as (
        select orders.order_id,
        Orders.customer_id,
        Orders.ORDER_DATE AS order_placed_at,
        Orders.order_status,
        p.total_amount_paid,
        p.payment_finalized_date,
        C.FIRST_NAME as customer_first_name,
        C.LAST_NAME as customer_last_name
    FROM {{ ref('stg_orders')}} as orders
    left join (
            select order_id
                , max(CREATED) as payment_finalized_date
                , sum(payment_AMOUNT) / 100.0 as total_amount_paid
            from {{ ref('stg_payments')}}
            where payment_STATUS <> 'fail'
            group by 1
            ) p ON orders.order_id = p.order_id
    left join {{ ref('stg_customers')}} C on orders.customer_id = C.customer_id 
)
select * from paid_orders
