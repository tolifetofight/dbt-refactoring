with source as
(
  select * from {{ source('stripe', 'payment')}}
    
)
, payment as(
  select id as payment_id
        , orderid as order_id
        , status as payment_status
        , round(amount/100.0,2) as payment_amount
        , created
        , paymentmethod as payment_method
  from source
)
select * from payment